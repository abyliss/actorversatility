import sys
import os
import numpy as np

sys.path.append('./bhtsne')
import bhtsne

result_dir = "modelfiles_agp_reldiv_rel"

for model_dir in next(os.walk(result_dir))[1]:
	print model_dir
	if not os.path.exists(os.path.join(result_dir, model_dir, "embed_tsne.csv")):
		print("no tsne in ", model_dir)
		mus = np.loadtxt(os.path.join(result_dir, model_dir, "embed_mu.csv"), delimiter=",")
		coord = bhtsne.run_bh_tsne(mus, initial_dims=mus.shape[1]);
		np.savetxt(os.path.join(result_dir, model_dir, "embed_tsne.csv"), coord, fmt='%10.10f', delimiter=',')