from evaluate_link_pred import Word2GM

if __name__ == '__main__':
    model_dir = '/mnt/scratch/boyang/actors/modelfiles_test_cosine/' \
        + 'www-agp-train2_t600_dim40_b128_min10_rms_spherical_wsame_lr0.15' \
        + '_margin4.0_drop0.6_clipTrue_0.0001-25.0_rel_noreg__12'
    w2gm = Word2GM(model_dir)
    w2gm.show_similarity_within_class(
        'p_3061_Ewan_McGregor', 'p_6193_Leonardo_DiCaprio', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_3894_Christian_Bale', 'p_3061_Ewan_McGregor', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_2712_Michael_Biehn', 'p_1736_James_Remar', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_1037_Harvey_Keitel', 'p_8349_Martin_Sheen', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_7036_Eric_Stoltz', 'p_521_Michael_J__Fox', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_586_Sean_Young', 'p_326_Kim_Basinger', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_516_Annette_Bening', 'p_1160_Michelle_Pfeiffer', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_16483_Sylvester_Stallone', 'p_776_Eddie_Murphy', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_4347_Frank_Sinatra', 'p_190_Clint_Eastwood', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_1813_Anne_Hathaway', 'p_25541_Katherine_Heigl', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_2227_Nicole_Kidman', 'p_1038_Jodie_Foster', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_15112_Tom_Selleck', 'p_3_Harrison_Ford', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_30614_Ryan_Gosling', 'p_13240_Mark_Wahlberg', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_15336_Dougray_Scott', 'p_6968_Hugh_Jackman', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_18288_Terrence_Howard', 'p_1896_Don_Cheadle', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_1579_Maggie_Gyllenhaal', 'p_3897_Katie_Holmes', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_819_Edward_Norton', 'p_103_Mark_Ruffalo', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_1892_Matt_Damon', 'p_17604_Jeremy_Renner', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_194_Richard_Harris', 'p_5658_Michael_Gambon', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_49_Maria_Bello', 'p_3293_Rachel_Weisz', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_7447_Alec_Baldwin', 'p_3_Harrison_Ford', 0, 3, 100, False)
    w2gm.show_similarity_within_class(
        'p_1231_Julianne_Moore', 'p_1038_Jodie_Foster', 0, 3, 100, False)
