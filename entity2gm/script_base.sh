#!/bin/bash
epoch=600
for iter in 1 2 3
do
  for dim in 30 40 50
  do
    for margin in 1 2 4
    do
      for mincount in 10
      do
        for batchsize in 128
        do
          for wout in "wsame"
          do
            for grad in "adagrad" "rms"
            do
              for spherical in "True" "False"
              do
                for dropout in 0.4 0.5 0.6
                do
                  for rel in "False"
                  do
                    for lr in 0.1 0.05 0.01
                    do
                      reg="noreg"
                      python entity2gm4.py --train_data data3/www-agp-train1 --iter ${iter} --num_mixtures 1 --spherical ${spherical} --embedding_size ${dim} --epochs_to_train ${epoch} --objective_threshold ${margin} --var_scale 0.05 --save_path modelfiles_norel/ --learning_rate ${lr} --${grad} --min_count ${mincount} --batch_size ${batchsize} --checkpoint_interval 1500 --dropout ${dropout} --concurrent_steps 4 --rel ${rel} 
                      
                      for reg_beta in 0.01 0.05
                      do
                        for reg_delta in 0.0 0.01 0.05
                        do
                          reg="reg_l1"
                          python entity2gm4.py --train_data data3/www-agp-train1 --iter ${iter} --num_mixtures 1 --spherical ${spherical} --embedding_size ${dim} --epochs_to_train ${epoch} --objective_threshold ${margin} --var_scale 0.05 --save_path modelfiles_norel/ --learning_rate ${lr} --${grad} --min_count ${mincount} --batch_size ${batchsize} --checkpoint_interval 1500 --dropout ${dropout} --concurrent_steps 4 --reg_beta ${reg_beta} --reg_delta ${reg_delta} --${reg} --rel ${rel}
                          
                          reg="reg_l2"
                          for reg_nu in 2 4 6
                          do
                            python entity2gm4.py --train_data data3/www-agp-train1 --iter ${iter} --num_mixtures 1 --spherical ${spherical} --embedding_size ${dim} --epochs_to_train ${epoch} --objective_threshold ${margin} --var_scale 0.05 --save_path modelfiles_norel/ --learning_rate ${lr} --${grad} --min_count ${mincount} --batch_size ${batchsize} --checkpoint_interval 1500 --dropout ${dropout} --concurrent_steps 4 --reg_beta ${reg_beta} --reg_delta ${reg_delta} --reg_nu ${reg_nu} --${reg} --rel ${rel}
                          done
                        done
                      done
                    done
                  done
                done
              done
            done
          done
        done
      done
    done
  done
done
#--save_path /mnt/data/hannah/modelfiles4/
#python entity2gm4.py --num_mixtures 1 --train_data data2/final-all-attr-char-train-1 --spherical --embedding_size 40 --epochs_to_train 200 --var_scale 0.05 --save_path modelfiles4/ --learning_rate 0.1  --adagrad --min_count 10 --batch_size 128 --max_to_keep 5 --checkpoint_interval 500 --dropout 0.5 --concurrent_steps 4 --rel --reg_l2

#python entity2gm4.py --num_mixtures 1 --train_data data3/www-agp-train1 --spherical --embedding_size 40 --epochs_to_train 6 --var_scale 0.05 --save_path modelfiles4/ --learning_rate 0.1  --adagrad --min_count 10 --batch_size 128 --max_to_keep 5 --checkpoint_interval 500 --dropout 0.5 --concurrent_steps 4 --rel --reg_l2