from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
import threading
import time
import math
from six.moves import xrange  # pylint: disable=redefined-builtin
import csv
from collections import Counter
from itertools import dropwhile
from tensorflow.contrib.lookup import *
from multiprocessing.pool import ThreadPool, Pool

# Retrict to CPU only
os.environ["CUDA_VISIBLE_DEVICES"] = ""

import numpy as np
import tensorflow as tf

flags = tf.app.flags

flags.DEFINE_string("save_path", None, "Directory to write the model and "
                    "training summaries. (required)")
flags.DEFINE_string("train_data", None, "Training text file. (required)")
flags.DEFINE_integer("embedding_size", 50, "The embedding dimension size.")
flags.DEFINE_integer("epochs_to_train", 5,
                     "Number of epochs to train. Each epoch processes the training data once "
                     "completely.")
flags.DEFINE_float("learning_rate", 0.2, "Initial learning rate.")

flags.DEFINE_integer("batch_size", 256,
                     "Number of training examples processed per step "
                     "(size of a minibatch).")
flags.DEFINE_integer("concurrent_steps", 8,
                     "The number of concurrent training steps.")
flags.DEFINE_integer("min_count", 5,
                     "The minimum number of word occurrences for it to be "
                     "included in the vocabulary.")
flags.DEFINE_float("subsample", 1e-3,
                   "Subsample threshold for word occurrence. Words that appear "
                   "with higher frequency will be randomly down-sampled. Set "
                   "to 0 to disable.")
flags.DEFINE_integer("statistics_interval", 5,
                     "Print statistics every n seconds.")
flags.DEFINE_integer("summary_interval", 5,
                     "Save training summary to file every n seconds (rounded "
                     "up to statistics interval).")
flags.DEFINE_integer("checkpoint_interval", 600,
                     "Checkpoint the model (i.e. save the parameters) every n "
                     "seconds (rounded up to statistics interval).")
flags.DEFINE_integer("num_mixtures", 2,
                     "Number of mixture component for Mixture of Gaussians")
flags.DEFINE_boolean("spherical", False,
                     "Whether the model should be spherical or diagonal"
                     "The default is spherical")

flags.DEFINE_float("var_scale", 0.05, "Variance scale")

flags.DEFINE_boolean("ckpt_all", False, "Keep all checkpoints"
                     "(Warning: This requires a large amount of disk space).")

#flags.DEFINE_float("norm_cap", 3.0,
#                   "The upper bound of norm of mean vector")
flags.DEFINE_boolean("normclip", False,
                     "Whether to perform norm clipping")   
flags.DEFINE_float("lower_sig", 0.0001, # e^-9.21 = 0.0001
                   "The lower bound for sigma element-wise")
flags.DEFINE_float("upper_sig", 100, # e^4.6 = 100
                   "The upper bound for sigma element-wise")
flags.DEFINE_float("mu_scale", 1.0,
                   "The average norm will be around mu_scale")

flags.DEFINE_float("objective_threshold", 1.0,
                   "The threshold for the objective")

flags.DEFINE_boolean("adagrad", False,
                     "Use Adagrad optimizer instead")
flags.DEFINE_boolean("adam", False,
                     "Use Adam optimizer instead")
flags.DEFINE_boolean("rms", False,
                     "Use RMSProp optimizer instead")
flags.DEFINE_boolean("cosine_schedule", False,
                     "Use the cosine learning rate schedule instead of the linear schedule")                     

flags.DEFINE_boolean("reg_l1", False,
                     "Use L1 regularization")
flags.DEFINE_boolean("reg_l2", False,
                     "Use L2 regularization")
flags.DEFINE_float("reg_beta", 0.01,
                   "beta parameter for regularizer")
flags.DEFINE_float("reg_gamma", 0.0,
                   "beta parameter for regularizer")
flags.DEFINE_float("reg_delta", 0.0,
                   "beta parameter for regularizer")
flags.DEFINE_float("reg_nu", 2.0,
                   "beta parameter for regularizer")
flags.DEFINE_float("dropout", 0.0,
                   "Use Dropout rate")

flags.DEFINE_boolean("rel", False,
                     "Use relation vector")

flags.DEFINE_float("loss_epsilon", 1e-4,
                   "epsilon parameter for loss function")

flags.DEFINE_boolean("constant_lr", False,
                     "Use constant learning rate")

flags.DEFINE_boolean("wout", False,
                     "Whether we would use a separate wout")
flags.DEFINE_boolean("wsame", False,
                     "Whether we would use a separate wout")

flags.DEFINE_boolean("max_pe", False,
                     "Using maximum of partial energy instead of the sum")

flags.DEFINE_integer("max_to_keep", 5,
                     "The maximum number of checkpoint files to keep")              

flags.DEFINE_integer("iter", -1,
                     "For expriment repetition")

flags.DEFINE_boolean("glove", False,
                     "Whether we would use a pretrained word vector")

flags.DEFINE_boolean("reldiv", False,
                     "Divide relation vector dimension")

flags.DEFINE_integer("a_dim", 25,
                     "Age hyperplan dimension")

flags.DEFINE_integer("g_dim", 5,
                     "Age hyperplan dimension")



FLAGS = flags.FLAGS


class Options(object):
    """Options used by our Word2MultiGauss model."""

    def __init__(self):
        # Model options.

        # Embedding dimension.
        self.emb_dim = FLAGS.embedding_size

        # Training options.
        # The training text file.
        self.train_data = FLAGS.train_data

        # The initial learning rate.
        self.learning_rate = FLAGS.learning_rate

        # Number of epochs to train. After these many epochs, the learning
        # rate decays linearly to zero and the training stops.
        self.epochs_to_train = FLAGS.epochs_to_train

        # Concurgnt training steps.
        self.concurrent_steps = FLAGS.concurrent_steps

        # Number of examples for one training step.
        self.batch_size = FLAGS.batch_size

        # The minimum number of word occurrences for it to be included in the
        # vocabulary.
        self.min_count = FLAGS.min_count

        # Subsampling threshold for word occurrence.
        self.subsample = FLAGS.subsample

        # How often to print statistics.
        self.statistics_interval = FLAGS.statistics_interval

        # How often to write to the summary file (rounds up to the nearest
        # statistics_interval).
        self.summary_interval = FLAGS.summary_interval

        # How often to write checkpoints (rounds up to the nearest statistics
        # interval).
        self.checkpoint_interval = FLAGS.checkpoint_interval

        #################################
        self.num_mixtures = FLAGS.num_mixtures  # incorporated. needs testing

        # upper bound of norm of mu
        # self.norm_cap = FLAGS.norm_cap        

        # whether to use spherical or diagonal covariance
        self.spherical = FLAGS.spherical  # default to False please
        if FLAGS.spherical:
            var = 'spherical'
        else:
            var = 'diagonal'

        self.var_scale = FLAGS.var_scale

        self.ckpt_all = FLAGS.ckpt_all

        self.mu_scale = FLAGS.mu_scale

        self.objective_threshold = FLAGS.objective_threshold

        self.adagrad = FLAGS.adagrad
        self.adam = FLAGS.adam
        self.rms = FLAGS.rms
        if FLAGS.adagrad:
            grad = 'adagrad'
        elif FLAGS.adam:
            grad = 'adam'
        elif FLAGS.rms:
            grad = 'rms'
        else:
            grad = 'sgd'
        self.cosine_schedule = FLAGS.cosine_schedule

        self.reg_l1 = FLAGS.reg_l1
        self.reg_l2 = FLAGS.reg_l2
        self.reg_beta = FLAGS.reg_beta
        self.reg_gamma = FLAGS.reg_gamma
        self.reg_delta = FLAGS.reg_delta
        self.reg_nu = FLAGS.reg_nu
        self.dropout = FLAGS.dropout
        if FLAGS.reg_l1:
            reg = 'l1_'+str(FLAGS.reg_beta)
        elif FLAGS.reg_l2:
            reg = 'l2_'+str(FLAGS.reg_beta)
        else:
            reg = 'noreg'

        self.rel = FLAGS.rel
        if FLAGS.rel:
            rel_str = 'rel'
        else:
            rel_str = 'norel'

        self.loss_epsilon = FLAGS.loss_epsilon

        self.constant_lr = FLAGS.constant_lr

        self.wout = FLAGS.wout
        if FLAGS.wout:
            wout = 'wout'
        else:
            wout = 'wsame'

        self.max_pe = FLAGS.max_pe

        self.max_to_keep = FLAGS.max_to_keep

        self.normclip = FLAGS.normclip

        # value clipping
        # element-wise lower bound for sigma
        self.lower_sig = FLAGS.lower_sig
        self.lower_logsig = math.log(self.lower_sig)
        # element-wise upper bound for sigma
        self.upper_sig = FLAGS.upper_sig
        self.upper_logsig = math.log(self.upper_sig)      

        # self.norm_cap = FLAGS.norm_cap
        
        self.reldiv = FLAGS.reldiv
        self.a_dim = FLAGS.a_dim
        self.g_dim = FLAGS.g_dim

        self.glove = FLAGS.glove
        if FLAGS.embedding_size != 50:
            self.glove = False

        # Where to write out summaries.
#    self.save_path = FLAGS.save_path
        self.save_path = FLAGS.save_path+'/'+FLAGS.train_data.split('/')[-1]+'_t'+str(FLAGS.epochs_to_train)+'_dim'+str(FLAGS.embedding_size)+'_b'+str(FLAGS.batch_size)+'_min'+str(FLAGS.min_count)+'_'+grad+'_'+var+'_'+wout+'_lr'+str(
            FLAGS.learning_rate) + '_margin' +str(FLAGS.objective_threshold)+'_drop' + str(FLAGS.dropout) + '_clip'+str(
            FLAGS.normclip)+'_'+str(self.lower_sig) + '-' + str(self.upper_sig) + '_' + rel_str+'_'+reg

        if FLAGS.glove:
            self.save_path = self.save_path+'_glove'
        if FLAGS.reldiv:
            self.save_path = self.save_path+'_' + \
                str(FLAGS.a_dim)+'-'+str(FLAGS.g_dim)+'-' + \
                str(FLAGS.embedding_size-FLAGS.a_dim-FLAGS.g_dim)
        if FLAGS.iter >= 0:
            self.save_path = self.save_path+'__'+str(FLAGS.iter)


class lockedgen:
    def __init__(self, gen):
        self.g = gen
        self.l = threading.Lock()

    def get(self):
        self.l.acquire()
        try:
            a = self.g.next()
        except StopIteration:
            a = None
        finally:
            self.l.release()
        return a


class Entity2GMTrainer():
    def __init__(self, options, session):
        self._options = options
        opts = self._options
        self._session = session

        dataset = [opts.train_data]
        self.build_vocab(dataset)
        self.build_graph(dataset)

        self.gen = lockedgen(self.data_iterator())
        self.save_vocab()

        # result = self._session.run(self._word2id.lookup(tf.constant(self._id2word)))
        # print(result)
        # print(self._id2class.eval())

    def build_vocab(self, filenames):
        c = Counter()
        v = Counter()
        for file in filenames:
            # with open(file, 'rb') as csvfile:
            with open(file, 'r') as csvfile:
                r = csv.reader(csvfile, delimiter=' ')
                for row in r:
                    c[row[0]] += 1
                    c[row[1]] += 1
                    for vec in row[2].split(','):
                        v[vec] += 1
        if self._options.min_count > 0:
            for key, count in dropwhile(lambda key_count: key_count[1] >= self._options.min_count, c.most_common()):
                del c[key]
        for key, count in dropwhile(lambda key_count: key_count[1] > 1, v.most_common()):
            del v[key]
        c = sorted(c.items())
        # c = map(list,zip(*c))
        c = zip(*c)
        # print(c)
        v = sorted(v.items())
        v = zip(*v)
        print(v)
        vocab_vecs = v[0]
        vocab_words = c[0]
        # vocab_freq = c[1]
        vocab_class = []
        for w in vocab_words:
            cl = -1
            if w.startswith('m_'):
                cl = 2
            elif w.startswith('k_'):
                cl = 1
            elif w.startswith('g_'):
                cl = 0
            elif w.startswith('p_'):
                cl = 3
            vocab_class.append(cl)
        vec_class = []
        for w in vocab_vecs:
            cl = -1
            if w.startswith('v_age_'):
                cl = 0
            elif w.startswith('v_gender'):
                cl = 1
            elif w.startswith('v_persona'):
                cl = 2
            vec_class.append(cl)
        # class_bin = tf.Variable(np.insert(np.cumsum(np.bincount(vocab_class)),0,0), name="class_bin", dtype=tf.int32)
        class_bin = np.insert(np.cumsum(np.bincount(vocab_class)), 0, 0)
        # word2id = tf.contrib.lookup.HashTable(tf.contrib.lookup.KeyValueTensorInitializer(vocab_words, range(len(vocab_words)), tf.string, tf.int64), -1)
        word2id_ = dict(zip(vocab_words, range(len(vocab_words))))
        vocab_freq = np.zeros(len(vocab_words))
        vec2id_ = dict(zip(vocab_vecs, range(len(vocab_vecs))))
        vec_freq = np.zeros(len(vocab_vecs))
        # corpus = []
        corpus = [[], [], []]
        for file in filenames:
            with open(file, 'rb') as csvfile:
                r = csv.reader(csvfile, delimiter=' ')
                for row in r:
                    if row[0] in word2id_ and row[1] in word2id_:
                        vocab_freq[word2id_[row[0]]] += 1
                        vocab_freq[word2id_[row[1]]] += 1
                        # corpus.append([word2id_[row[0]], word2id_[row[1]]])
                        corpus[0].append(word2id_[row[0]])
#            corpus[0].append(word2id_[row[1]])
                        corpus[1].append(word2id_[row[1]])
#            corpus[1].append(word2id_[row[0]])
                        this_vec = []
                        for vec in row[2].split(','):
                            if vec in vec2id_:
                                this_vec.append(vec2id_[vec])
                                vec_freq[vec2id_[vec]] += 1
                        corpus[2].append(this_vec)
        max_vec = max([len(corpus[2][i]) for i in range(len(corpus[2]))])
        for i in range(len(corpus[2])):
            corpus[2][i].extend([-1]*(max_vec-len(corpus[2][i])))

        if self._options.glove:
            self._glove = dict()
            with open('data3/glove_keyword.csv', 'rb') as csvfile:
                r = csv.reader(csvfile, delimiter=',')
                for row in r:
                    self._glove[row[0]] = [float(x) for x in row[1:]]

        self._id2word = vocab_words
        self._vocab_freq = vocab_freq
        # self._id2class = tf.Variable(vocab_class, name="id2class")
        self._id2class = vocab_class
        self._class_bin = class_bin
        # self._word2id = word2id
        self._word2id = word2id_
        self._corpus = corpus
        self._id2vec = vocab_vecs
        self._vec2id = vec2id_
        self._vid2class = vec_class
        opts = self._options
        opts.vocab_words = self._id2word
        opts.vocab_counts = self._vocab_freq
        opts.vocab_size = len(opts.vocab_words)
        opts.words_per_epoch = sum(opts.vocab_counts)
        opts.vocab_class = vocab_class
        opts.max_step = np.floor(
            opts.epochs_to_train * opts.words_per_epoch / (opts.batch_size*2.0))
        opts.vocab_vecs = self._id2vec
        opts.vec_size = len(opts.vocab_vecs)
        opts.vec_counts = self._vocab_freq
        opts.vec_freq = vec_freq
        opts.max_vec = max_vec
        print(corpus[0][-10:-1])
        print(corpus[1][-10:-1])
        print(corpus[2][-10:-1])
        print(corpus[2][0:10])
        print(len(corpus[0]), len(corpus[1]), len(corpus[2]))
        print(vec_class)

    def data_iterator(self):
        """ A simple data iterator """
        opts = self._options
        examples = np.array(self._corpus[0])
        labels = np.array(self._corpus[1])
        relations = np.array(self._corpus[2])

        current_idx = 0

        idxs = np.arange(0, len(examples))
        np.random.shuffle(idxs)
        shuf_examples = examples[idxs]
        shuf_labels = labels[idxs]
        shuf_rels = relations[idxs]
        for _ in xrange(np.int(opts.max_step)+1):
            #      print(_,' / ',opts.max_step)
            #      print('===========current_idx============= ', current_idx)
            if current_idx + opts.batch_size < len(examples):
                example_batch = shuf_examples[current_idx:current_idx +
                                              opts.batch_size]
                label_batch = shuf_labels[current_idx:current_idx +
                                          opts.batch_size]
                rel_batch = shuf_rels[current_idx:current_idx +
                                      opts.batch_size]
                current_idx += opts.batch_size
            else:
                example_batch = shuf_examples[current_idx:]
                label_batch = shuf_labels[current_idx:]
                rel_batch = shuf_rels[current_idx:]

                self._epoch += 1

                np.random.shuffle(idxs)
                shuf_examples = examples[idxs]
                shuf_labels = labels[idxs]
                shuf_rels = relations[idxs]

                # fills the batch with data from the next epoch
                # Not sure why this is necessary -- Albert Li, Jul 26 2018
                current_idx = opts.batch_size - len(example_batch)
                example_batch = np.append(
                    example_batch, shuf_examples[:current_idx])
                label_batch = np.append(label_batch, shuf_labels[:current_idx])
                rel_batch = np.concatenate(
                    (rel_batch, shuf_rels[:current_idx]), axis=0)

            # randomly generate the next negative batch
            neg_batch = np.empty_like(label_batch)
            for i in xrange(opts.batch_size):
                neg_batch[i] = np.random.randint(
                    self._class_bin[self._id2class[label_batch[i]]], self._class_bin[self._id2class[label_batch[i]]+1])
            yield example_batch, label_batch, neg_batch, rel_batch

    def calculate_loss(self, word_idxs, pos_idxs, neg_idxs, rel_idxs):
        # This is two methods in one (forward and nce_loss)
        self.global_step = tf.Variable(0, name="global_step", trainable=False)
        opts = self._options

        vid2class = self._vid2class
        a_dim = opts.a_dim
        g_dim = opts.g_dim
        #####################################################
        # the model parameters
        vocabulary_size = opts.vocab_size
        vec_size = opts.vec_size
        embedding_size = opts.emb_dim
        batch_size = opts.batch_size

        #norm_cap = opts.norm_cap

        
        # self.min_sig = lower_sig
        # self.max_sig = upper_sig

        num_mixtures = opts.num_mixtures
        spherical = opts.spherical
        objective_threshold = opts.objective_threshold

        # the model parameters
        mu_scale = opts.mu_scale*math.sqrt(3.0/(1.0*embedding_size))
        mus = tf.Variable(tf.random_uniform(
            [vocabulary_size, num_mixtures, embedding_size], -mu_scale, mu_scale), name='mu')
        if opts.rel:
            vec_scale = mu_scale * 1.0
            vecs = tf.Variable(tf.random_uniform(
                [vec_size, embedding_size], -vec_scale, vec_scale), name='vec_b4mask')
            if opts.reldiv:
                vmask_array = np.zeros((vec_size, embedding_size))
                print(vid2class)
                for vi in range(0, vec_size):
                    if vid2class[vi] == -1:
                        vmask_array[vi] = np.ones((embedding_size))
                    elif vid2class[vi] == 0:
                        vmask_array[vi][0:a_dim] = 1
                    elif vid2class[vi] == 1:
                        vmask_array[vi][a_dim:a_dim+g_dim] = 1
                    elif vid2class[vi] == 2:
                        vmask_array[vi][a_dim+g_dim:40] = 1
                print(vmask_array.shape)
                print(vmask_array)
            else:
                vmask_array = np.ones((vec_size, embedding_size))
            vmasks = tf.constant(
                vmask_array, dtype=tf.float32, name='vec_mask')
            vecs = tf.multiply(vecs, vmasks, name='vec')
            print(vecs.get_shape().as_list())
        if opts.wout:
            mus_out = tf.Variable(tf.random_uniform(
                [vocabulary_size, num_mixtures, embedding_size], -mu_scale, mu_scale), name='mu_out')
        # This intialization makes the variance around 1
        var_scale = opts.var_scale
        logvar_scale = math.log(var_scale)
        print('mu_scale = {} var_scale = {} logvar_scale = {}'.format(
            mu_scale, var_scale, logvar_scale))
        if spherical:
            logsigs = tf.Variable(tf.random_uniform([vocabulary_size, num_mixtures, 1],
                                                    logvar_scale, logvar_scale), name='sigma')
            if opts.wout:
                logsigs_out = tf.Variable(tf.random_uniform([vocabulary_size, num_mixtures, 1],
                                                            logvar_scale, logvar_scale), name='sigma_out')

        else:
            logsigs = tf.Variable(tf.random_uniform([vocabulary_size, num_mixtures, embedding_size],
                                                    logvar_scale, logvar_scale), name='sigma')
            if opts.wout:
                logsigs_out = tf.Variable(tf.random_uniform([vocabulary_size, num_mixtures, embedding_size],
                                                            logvar_scale, logvar_scale), name='sigma_out')

        mixture = tf.Variable(tf.random_uniform(
            [vocabulary_size, num_mixtures], 0, 0), name='mixture')
        if opts.wout:
            mixture_out = tf.Variable(tf.random_uniform(
                [vocabulary_size, num_mixtures], 0, 0), name='mixture_out')

        if not opts.wout:
            mus_out = mus
            logsigs_out = logsigs
            mixture_out = mixture

        zeros_vec = tf.zeros([batch_size], name='zeros')
        self._mus = mus
        self._logsigs = logsigs
        if opts.rel:
            self._vecs = vecs

        def log_energy(mu1, sig1, mix1, mu2, sig2, mix2, mask):
            # need to pass mix that's compatible!

            def partial_logenergy(cl1, cl2):
                m1 = mu1[:, cl1, :]
                m2 = mu2[:, cl2, :]
                s1 = sig1[:, cl1, :]
                s2 = sig2[:, cl2, :]
                if opts.dropout > 0.0:
                    m1 = tf.multiply(m1, mask) * 1/(1-opts.dropout)
                    m2 = tf.multiply(m2, mask) * 1/(1-opts.dropout)
                    if not opts.spherical:
                        s1 = tf.multiply(s1, mask) * 1/(1-opts.dropout)
                        s2 = tf.multiply(s2, mask) * 1/(1-opts.dropout)
                # m1 = tf.Print(m1, data=[tf.reduce_sum(mask)], message="m1 size: ")
                # m2 = tf.Print(m2, data=[mask[1:10]], message="m1 size: ")
                # s1 = tf.Print(m1, data=[tf.size(s1)], message="s1 size: ")
                with tf.name_scope('partial_logenergy') as scope:
                    _a = tf.add(s1, s2)  # should be do max add for stability?
                    # _a = tf.Print(_a, data=[tf.shape(_a)], message="_a size: ")
                    epsilon = opts.loss_epsilon

                    if spherical:
                        logdet = embedding_size * \
                            tf.log(epsilon + tf.squeeze(_a))
                    else:
                        logdet = tf.reduce_sum(
                            tf.log(epsilon + _a), reduction_indices=1, name='logdet')
                    ss_inv = 1./(epsilon + _a)
                    diff = tf.subtract(m1, m2)
                    # diff = tf.Print(diff, data=[tf.shape(diff)], message="diff size: ")
                    exp_term = tf.reduce_sum(
                        diff*ss_inv*diff, reduction_indices=1, name='expterm')
                    pe = -0.5*logdet - 0.5*exp_term
                    return pe

            with tf.name_scope('logenergy') as scope:
                log_e_list = []
                mix_list = []
                for cl1 in xrange(num_mixtures):
                    for cl2 in xrange(num_mixtures):
                        log_e_list.append(partial_logenergy(cl1, cl2))
                        mix_list.append(mix1[:, cl1]*mix2[:, cl2])
                log_e_pack = tf.stack(log_e_list)
                log_e_max = tf.reduce_max(log_e_list, reduction_indices=0)

                if opts.max_pe:
                    # Ben A: got this warning for max_pe
                    # UserWarning:
                    # Convering sparse IndexedSlices to a dense Tensor of unknown shape. This may consume a large amount of memory.
                    log_e_argmax = tf.argmax(log_e_list, dimension=0)
                    log_e = log_e_max*tf.gather(mix_list, log_e_argmax)
                else:
                    mix_pack = tf.stack(mix_list)
                    log_e = tf.log(tf.reduce_sum(
                        mix_pack*tf.exp(log_e_pack-log_e_max), reduction_indices=0))
                    log_e += log_e_max
                return log_e

        def Lfunc(word_idxs, pos_idxs, neg_idxs, rel_idxs):
            with tf.name_scope('LossCal') as scope:
                if opts.rel:
                    # divide hyperplane
                    # sum vecs
                    #          rel_idxs = tf.Print(rel_idxs, data=[vecs[38:40]], message="rel_idxs0: " , summarize=10)
                    rel_idxs_bin = tf.one_hot(rel_idxs, depth=vec_size)
#          rel_idxs_bin = tf.Print(rel_idxs_bin, data=[vid2class], message="rel_idxs1: " , summarize=30)
#          rel_idxs_bin = tf.Print(rel_idxs_bin, data=[rel_idxs[0][1]], message="rel_idxs1: " , summarize=30)
                    rel_idxs_bin = tf.reduce_sum(rel_idxs_bin, 1)
#          rel_idxs_bin = tf.Print(rel_idxs_bin, data=[rel_idxs[0]], message="rel_idxs2: " , summarize=30)
                    rel_embed = tf.matmul(rel_idxs_bin, vecs, name='MuRel')
#          rel_embed = tf.Print(rel_embed, data=[rel_embed[0:1]], message="rel_idxs3: " , summarize=30)
#          rel_embed = tf.Print(rel_embed, data=[tf.shape(rel_embed)], message="reldim1: " , summarize=30)
                    rel_embed = tf.expand_dims(rel_embed, 1)  # /vec_size
#          rel_embed = tf.Print(rel_embed, data=[tf.shape(rel_embed)], message="reldim2: " , summarize=30)
                    rel_embed = tf.tile(rel_embed, [1, num_mixtures, 1])
#          rel_embed = tf.Print(rel_embed, data=[tf.shape(rel_embed)], message="reldim3: " , summarize=30)


##        rel_idxs = tf.Print(rel_idxs, data=[rel_idxs[0:1]], message="rel_idxs: ")
##        rel_idxs = tf.Print(rel_idxs, data=[rel_idxs[1:2]], message="rel_idxs: ")
##        zeros = -tf.ones_like(rel_idxs, name='Zeros')
##        zeros = tf.Print(zeros, data=[zeros[0:1]], message="zeros: ")
##        zeros = tf.Print(zeros, data=[zeros[1:2]], message="zeros: ")
##        nonzero = tf.not_equal(rel_idxs, zeros, name='Nonzero')
##        nonzero = tf.Print(nonzero, data=[nonzero[0:1]], message="nonzero: ")
##        nonzero = tf.Print(nonzero, data=[nonzero[1:2]], message="nonzero: ")
##        rel_idxs = tf.boolean_mask(rel_idxs, nonzero, name='RelIdx')
##        rel_idxs = tf.Print(rel_idxs, data=[rel_idxs[0:1]], message="rel_idxs: ")
##        rel_idxs = tf.Print(rel_idxs, data=[rel_idxs[1:2]], message="rel_idxs: ")
##        rel_embed = tf.nn.embedding_lookup(vecs, rel_idxs, name='MuRel')
                mu_embed_b4 = tf.nn.embedding_lookup(
                    mus, word_idxs, name='MuWord')
                if opts.rel:
                    print("Using Relation information!")
#          mu_embed = tf.Print(mu_embed, data=[logsigs[0:2]], message="mudim: " , summarize=30)
#          mu_embed = tf.Print(mu_embed, data=[logsigs_out[0:2]], message="reldim: " , summarize=30)
                    mu_embed = tf.add(mu_embed_b4, rel_embed, name="MuWordRel")
#          mu_embed = tf.Print(mu_embed, data=[tf.shape(rel_embed)], message="finalmudim: " , summarize=30)
#          mu_embed = tf.Print(mu_embed, data=[tf.shape(mu_embed)], message="rel_idxs4: ")
                else:
                    mu_embed = mu_embed_b4
                    
                mu_embed_pos = tf.nn.embedding_lookup(
                    mus_out, pos_idxs, name='MuPos')
                mu_embed_neg = tf.nn.embedding_lookup(
                    mus_out, neg_idxs, name='MuNeg')
                sig_embed = tf.exp(tf.nn.embedding_lookup(
                    logsigs, word_idxs), name='SigWord')
                sig_embed_pos = tf.exp(tf.nn.embedding_lookup(
                    logsigs_out, pos_idxs), name='SigPos')
                sig_embed_neg = tf.exp(tf.nn.embedding_lookup(
                    logsigs_out, neg_idxs), name='SigNeg')

                mix_word = tf.nn.softmax(tf.nn.embedding_lookup(
                    mixture, word_idxs), name='MixWord')
                mix_pos = tf.nn.softmax(tf.nn.embedding_lookup(
                    mixture_out, pos_idxs), name='MixPos')
                mix_neg = tf.nn.softmax(tf.nn.embedding_lookup(
                    mixture_out, neg_idxs), name='MixNeg')

                mask = tf.random_uniform([batch_size*embedding_size])
                # mask = tf.Print(mask, data=[mask[1:10]], message="mask: ")
                mask = tf.greater(mask, opts.dropout)
                # mask = tf.Print(mask, data=[mask[1:10]], message="mask2: ")
                mask = tf.cast(mask, tf.float32)
                mask = tf.reshape(mask, [batch_size, embedding_size])
                # mask = tf.Print(mask, data=[mask[1:10]], message="mask3: ")

                epos = log_energy(mu_embed, sig_embed, mix_word,
                                  mu_embed_pos, sig_embed_pos, mix_pos, mask)
                eneg = log_energy(mu_embed, sig_embed, mix_word,
                                  mu_embed_neg, sig_embed_neg, mix_neg, mask)
                loss_indiv = tf.maximum(
                    zeros_vec, objective_threshold - epos + eneg, name='CalculateIndividualLoss')

                if opts.reg_l1:
                    print("Using L1 Regularizer!")
                    reg = tf.reduce_sum(tf.abs(mu_embed), [1, 2]) + tf.reduce_sum(
                        tf.abs(mu_embed_pos), [1, 2]) + tf.reduce_sum(tf.abs(mu_embed_neg), [1, 2])
                    reg_sig = tf.reduce_sum(tf.abs(sig_embed-1), [1, 2]) + tf.reduce_sum(tf.abs(
                        sig_embed_pos-1), [1, 2]) + tf.reduce_sum(tf.abs(sig_embed_neg-1), [1, 2])

                    if opts.rel:
                        reg_vec = tf.reduce_sum(tf.abs(rel_embed), [1, 2])
                        loss = tf.reduce_mean(
                            loss_indiv + opts.reg_beta * reg + opts.reg_gamma * reg_vec + opts.reg_delta * reg_sig, name='AveLoss')
                    else:
                        loss = tf.reduce_mean(
                            loss_indiv + opts.reg_beta * reg + opts.reg_delta * reg_sig, name='AveLoss')
                elif opts.reg_l2:
                    print("Using L2 Regularizer!")
                    reg = tf.reduce_sum(tf.square(mu_embed), [1, 2]) + tf.reduce_sum(tf.square(
                        mu_embed_pos), [1, 2]) + tf.reduce_sum(tf.square(mu_embed_neg), [1, 2])
                    # old sigma reg (l2)
#          reg_sig = tf.reduce_sum(tf.square(sig_embed-1), [1,2]) + tf.reduce_sum(tf.square(sig_embed_pos-1), [1,2]) + tf.reduce_sum(tf.square(sig_embed_neg-1), [1,2])
                    # new sigma reg wishart
                    reg_sig = opts.reg_nu*(tf.reduce_sum(1/sig_embed, [1, 2])+tf.reduce_sum(1/sig_embed_neg, [1, 2])+tf.reduce_sum(1/sig_embed_pos, [1, 2])) + (
                        1+opts.reg_nu) * (tf.reduce_sum(tf.log(sig_embed), [1, 2])+tf.reduce_sum(tf.log(sig_embed_pos), [1, 2])+tf.reduce_sum(tf.log(sig_embed_neg), [1, 2]))

                    if opts.rel:
                        reg_vec = tf.reduce_sum(tf.square(rel_embed), [1, 2])
                        loss = tf.reduce_mean(
                            loss_indiv + opts.reg_beta * reg + opts.reg_gamma * reg_vec + opts.reg_delta * reg_sig, name='AveLoss')
                    else:
                        loss = tf.reduce_mean(
                            loss_indiv + opts.reg_beta * reg + opts.reg_delta * reg_sig, name='AveLoss')
                else:
                    print("Using no Regularizer!")
                    loss = tf.reduce_mean(loss_indiv, name='AveLoss')
                return loss

        loss = Lfunc(word_idxs, pos_idxs, neg_idxs, rel_idxs)
        tf.summary.scalar('loss', loss)

        return loss

    def clip_sig(self, opts):
        ''' use to clip the sigmas to between the max and the min
            called only when norm_clip was specified. 
        '''
        min = tf.constant(opts.lower_logsig)
        max = tf.constant(opts.upper_logsig)
        return tf.clip_by_value(self._logsigs, min, max)

    # def clip_ops_graph(self, word_idxs, pos_idxs, neg_idxs):
    #     ''' use to clip embeddings?
    #         called only when norm_clip was specified.
    #     '''
    #     def clip_val_ref(embedding, idxs):
    #         with tf.name_scope('clip_val'):
    #             to_update = tf.nn.embedding_lookup(embedding, idxs)
    #             to_update = tf.maximum(self.lower_logsig, tf.minimum(
    #                 self.upper_logsig, to_update))
    #             return tf.scatter_update(embedding, idxs, to_update)

    #     clip4 = clip_val_ref(self._logsigs, word_idxs)
    #     clip5 = clip_val_ref(self._logsigs, pos_idxs)
    #     clip6 = clip_val_ref(self._logsigs, neg_idxs)

    #     return [clip4, clip5, clip6]

    def optimize(self, loss):
        """Build the graph to optimize the loss function."""

        # Optimizer nodes.
        # Linear learning rate decay.
        opts = self._options
        if opts.constant_lr:
            self._lr = tf.constant(opts.learning_rate)
        else:
            lr = opts.learning_rate * \
                tf.maximum(0.0001, 1.0 - tf.cast(self.global_step,
                                                 tf.float32) / opts.max_step)
            self._lr = lr
        optimizer = tf.train.GradientDescentOptimizer(self._lr)
        train = optimizer.minimize(loss,
                                   global_step=self.global_step,
                                   gate_gradients=optimizer.GATE_NONE)
        self._train = train

    def optimize_rms(self, loss):
        opts = self._options
        if opts.constant_lr:
            self._lr = tf.constant(opts.learning_rate)
        else:
            lr = opts.learning_rate * \
                tf.maximum(0.0001, 1.0 - tf.cast(self.global_step,
                                                 tf.float32) / opts.max_step)
            self._lr = lr
        optimizer = tf.train.RMSPropOptimizer(self._lr)
        train = optimizer.minimize(loss, global_step=self.global_step,
                                   gate_gradients=optimizer.GATE_NONE)
        self._train = train

    def optimize_rms_cosine(self, loss):
        opts = self._options
        if opts.constant_lr:
            self._lr = tf.constant(opts.learning_rate)
        else:
            lr = opts.learning_rate * \
                tf.maximum(0.0001, tf.cos(math.pi * 
                    tf.cast(self.global_step, tf.float32) / opts.max_step) / 2.0 + 0.5)
            self._lr = lr
        optimizer = tf.train.RMSPropOptimizer(self._lr)
        train = optimizer.minimize(loss, global_step=self.global_step,
                                   gate_gradients=optimizer.GATE_NONE)
        self._train = train

    def optimize_adam(self, loss):
        opts = self._options
        if opts.constant_lr:
            self._lr = tf.constant(opts.learning_rate)
        else:
            lr = opts.learning_rate * \
                tf.maximum(0.0001, 1.0 - tf.cast(self.global_step,
                                                 tf.float32) / opts.max_step)
            self._lr = lr
        optimizer = tf.train.AdamOptimizer(self._lr)
        train = optimizer.minimize(loss, global_step=self.global_step,
                                   gate_gradients=optimizer.GATE_NONE)
        self._train = train
    
    def optimize_adagrad_cosine(self, loss):
        opts = self._options
        if opts.constant_lr:
            self._lr = tf.constant(opts.learning_rate)
        else:
            lr = opts.learning_rate * \
                tf.maximum(0.0001, tf.cos(math.pi * 
                    tf.cast(self.global_step, tf.float32) / opts.max_step) / 2.0 + 0.5)
            self._lr = lr
        optimizer = tf.train.AdagradOptimizer(self._lr)
        train = optimizer.minimize(loss,
                                   global_step=self.global_step,
                                   gate_gradients=optimizer.GATE_NONE)
        self._train = train

    def optimize_adagrad(self, loss):
        opts = self._options
        if opts.constant_lr:
            self._lr = tf.constant(opts.learning_rate)
        else:
            lr = opts.learning_rate * \
                tf.maximum(0.0001, 1.0 - tf.cast(self.global_step,
                                                 tf.float32) / opts.max_step)
            self._lr = lr
        optimizer = tf.train.AdagradOptimizer(self._lr)
        train = optimizer.minimize(loss,
                                   global_step=self.global_step,
                                   gate_gradients=optimizer.GATE_NONE)
        self._train = train

    def glove_init(self):
        idx_subset = []
        glove_subset = []
        for i in xrange(len(self._id2word)):
            word = self._id2word[i]
            if self._glove.has_key(word):
                idx_subset.append([i, 0])
                glove_subset.append(self._glove[word])
        return tf.scatter_nd_update(self._mus, idx_subset, glove_subset)

    def build_graph(self, dataset):
        opts = self._options
        print("Data file: ", opts.train_data)
        print("Vocab size: ", opts.vocab_size)
        print("Words per epoch: ", opts.words_per_epoch)

        self._row_pos = 0
        self._col_pos = 0
        self._epoch = 0

        # self._examples, self._labels, self._neg_idxs = self.input_pipeline(dataset)
        # loss = self.calculate_loss(self._examples, self._labels, self._neg_idxs)
        self._examples = tf.placeholder(tf.int32, shape=[opts.batch_size])
        self._labels = tf.placeholder(tf.int32, shape=[opts.batch_size])
        self._neg_idxs = tf.placeholder(tf.int32, shape=[opts.batch_size])
        self._rel_idxs = tf.placeholder(
            tf.int32, shape=[opts.batch_size, opts.max_vec], name="ohohoh")
        loss = self.calculate_loss(
            self._examples, self._labels, self._neg_idxs, self._rel_idxs)
        self._loss = loss

        if opts.normclip:
            self._clip_ops = self.clip_sig(opts)
            print('clipping with ' + str(opts.lower_sig) + ' to ' + str(opts.upper_sig))

        if opts.adagrad:
            if opts.cosine_schedule:
                print("Using Adagard with cosine schedule")
                self.optimize_adagrad_cosine(loss)
            else:
                print("Using Adagrad as the optimizer!")
                self.optimize_adagrad(loss)
        elif opts.adam:
            print("Using Adam as an optimizer!")
            self.optimize_adam(loss)
        elif opts.rms:
            if opts.cosine_schedule:
                print("Using RMSProp with cosine schedule")
                self.optimize_rms_cosine(loss)
            else:
                print("Using RMSProp as the optimizer")
                self.optimize_rms(loss)
        else:
            # Using Standard SGD
            self.optimize(loss)

        # TODO: clip the covariance to betweem a minimum and a maximum

        # Properly initialize all variables.
        self.check_op = tf.add_check_numerics_ops()

        tf.local_variables_initializer().run()
        tf.global_variables_initializer().run()
        # self._word2id.init.run()

        if self._options.glove:
            print("Using pre-trained GLOVE vector!")
            self._session.run(self.glove_init())

        try:
            print('Try using saver version v2')
            self.saver = tf.train.Saver(
                write_version=tf.train.SaverDef.V2, max_to_keep=opts.max_to_keep)
        except:
            print('Default to saver version v1')
            self.saver = tf.train.Saver(max_to_keep=opts.max_to_keep)

    def save_vocab(self):
        opts = self._options
        with open(os.path.join(opts.save_path, "vocab.txt"), "w") as f:
            for i in xrange(opts.vocab_size):
                vocab_words = tf.compat.as_text(
                    opts.vocab_words[i]).encode("utf-8")
                f.write("%s %d\n" % (vocab_words,
                                     opts.vocab_counts[i]))
        with open(os.path.join(opts.save_path, "class.txt"), "w") as f:
            for i in xrange(opts.vocab_size):
                f.write("%d\n" % (opts.vocab_class[i]))
        with open(os.path.join(opts.save_path, "relation.txt"), "w") as f:
            for i in xrange(opts.vec_size):
                vocab_vecs = tf.compat.as_text(
                    opts.vocab_vecs[i]).encode("utf-8")
                f.write("%s %d\n" % (vocab_vecs, opts.vec_freq[i]))

    def _train_thread_body(self, loss_list):

        # print("+++++ start -- id: ", threading.current_thread().getName())
        # print(loss_list)

        opts = self._options
        # initial_step = self._session.run(self.global_step)
        initial_epoch = self._epoch
        loop = True
        total_loss = 0.0
        n = 0
        while loop:
            opts.tmp[threading.current_thread().getName()] = time.time()
            # This is where the optimizer that minimizes loss (self._train) is run
            if not self._options.normclip:
                # print(self._row_pos)
                # word_idxs, pos_idxs, neg_idxs = self.generate_batch(threading.current_thread().getName())
                opts.tmp[threading.current_thread().getName()] = time.time()
                next_batch = self.gen.get()
                # print('next batch size: ' + str(len(next_batch)))
                # print('next batch size: ' + str(len(next_batch[0])))
                if next_batch is not None:
                    word_idxs, pos_idxs, neg_idxs, rel_idxs = next_batch
                    _, current_step, loss = self._session.run([self._train, self.global_step, self._loss], feed_dict={
                                                              self._examples: word_idxs, self._labels: pos_idxs, self._neg_idxs: neg_idxs, self._rel_idxs: rel_idxs})
                    total_loss += loss
                    n += 1
                else:
                    loop = False
                opts.tmp[threading.current_thread().getName()] = time.time()

            else:
                opts.tmp[threading.current_thread().getName()] = time.time()
                next_batch = self.gen.get()
                if next_batch is not None:
                    word_idxs, pos_idxs, neg_idxs, rel_idxs = next_batch
                    _, current_step, loss, _ = self._session.run([self._train, self.global_step, self._loss, self._clip_ops], feed_dict={
                        self._examples: word_idxs, self._labels: pos_idxs, self._neg_idxs: neg_idxs, self._rel_idxs: rel_idxs})
                    total_loss += loss
                    n += 1
                else:
                    loop = False

            if loop:
                epoch = np.floor(current_step * opts.batch_size *
                                 2/opts.words_per_epoch)
                # print('current step : ' + str(current_step))
                # print('opts.batch_size : ' + str(opts.batch_size))
                # print('opts.words_per_epoch : ' + str(opts.words_per_epoch))
                if initial_epoch != epoch:
                    # print(str(initial_epoch) + ' != ' + str(epoch))
                    loop = False

        loss_list.append(total_loss / n)

    def train_one_epoch(self, logfile):
        """Train the model."""
        opts = self._options
        opts.t0 = time.time()
        opts.last_step = 0
        opts.tmp = dict()

        lr = self._session.run(self._lr)
        # print('lr: ' + str(lr))
        workers = []

        loss_list = [[]]
        for _ in xrange(opts.concurrent_steps):
            t = threading.Thread(
                target=self._train_thread_body, args=loss_list)
            t.start()
            workers.append(t)

        for t in workers:
            t.join()

        # if opts.normclip:
        #     self._session.run(self._clip_ops, feed_dict={})

        loss = 0.0
        for i in loss_list[0]:
            loss += float(i)
        loss = loss / opts.concurrent_steps

        g_step = self._session.run(self.global_step)

        print('steps taken: ' + str(g_step) +
              ', lr = ' + str(lr) + ' loss: %0.6f' % loss)
        logfile.write(str(g_step) +
                      ', ' + str(lr) + ', %0.6f\n' % loss)


def main(_):
    if not FLAGS.train_data or not FLAGS.save_path:
        print("--train_data and --save_path must be specified.")
        sys.exit(1)
    opts = Options()
    if not os.path.exists(opts.save_path):
        print('Creating new directory', opts.save_path)
        os.makedirs(opts.save_path)
    else:
        print('The directory already exists', opts.save_path)
    print('Saving results to {}'.format(opts.save_path))

    # with open('logfiles.txt', 'a') as outfile:
    #     outfile.write('Saving results to {} \n'.format(opts.save_path))
    if os.path.isfile(os.path.join(opts.save_path, 'loss.log')):
        f = open(os.path.join(opts.save_path, 'loss.log'), 'a')
    else:
        f = open(os.path.join(opts.save_path, 'loss.log'), 'w')
        f.write('epoch, steps, lr, loss\n')

    with tf.Graph().as_default(), tf.Session() as sess:
        with tf.device("/cpu:0"):

            model = Entity2GMTrainer(opts, sess)
            last_model = tf.train.latest_checkpoint(opts.save_path)
            # print('max steps:')
            # print(opts.max_step)

            if last_model is None:
                model.current_step = 0
                start_epoch = 0
            else:
                print('loading: ' + last_model)
                model.saver.restore(sess, last_model)
                model.current_step = sess.run(model.global_step)
                last_epoch_str = last_model[last_model.rfind('-')+1:]
                last_epoch = int(last_epoch_str)
                start_epoch = last_epoch + 1
                model._epoch = start_epoch

            # opts.epochs_to_train = start_epoch + 20
            # coord = tf.train.Coordinator()
            # threads = tf.train.start_queue_runners(coord=coord)

            model._writer = csv.writer(f, quoting=csv.QUOTE_NONE)

            t0 = time.time()
            for current_e in xrange(start_epoch, opts.epochs_to_train):
                f.write('%d, ' % current_e)
                print('epoch %d' % current_e)
                model.train_one_epoch(f)  # Process one epoch
                f.flush()
                if (current_e + 1) % 20 == 0 and current_e > 0:
                    print('train time so far:' + str(time.time()-t0))
                    print('saving the checkpoint at epoch' +
                          str(current_e) + ' to ' + opts.save_path)
                    model.saver.save(sess, os.path.join(
                        opts.save_path, "model.ckpt"), global_step=current_e)

            print('total train time', time.time()-t0)
            print(model._epoch)
            f.write('TERMINATE\n')
            f.close()

        # Perform a final save.
        model.saver.save(sess,
                         os.path.join(opts.save_path, "model.ckpt"),
                         global_step=opts.epochs_to_train-1)


if __name__ == "__main__":
    tf.app.run()
