import os


def split_filenames(fn):
    ind = fn.rfind('.')
    return (fn[0:ind], fn[ind+1:])


dir = './modelfiles4'
iter = os.walk(dir)
number_kept = 0
number_delete = 0

bad_dir_file = open('bad_dir.txt', 'w')

for folder in iter:
    folder_name = folder[0]
    # print(folder_name)
    files = folder[2]
    keep = False
    for file in files:
        name, ext = split_filenames(file)
        # print(name)
        # print(ext)
        if (ext == 'meta' or ext.startswith('data') or ext == 'index'):
            epoch = int(name[-3:])
            # print(epoch)
            if epoch < 500:
                print('delete: ' + name)
                os.remove(os.path.join(folder_name, file))
            elif epoch == 599:
                keep = True
                number_kept += 1

    if not keep and folder_name != dir:
        print('removing ' + folder_name)
        # os.removedirs(folder_name)
        number_delete += 1
        bad_dir_file.write(folder_name + '\n')

print('delete: ' + str(number_delete) + ", keep: " + str(number_kept))
bad_dir_file.close()
