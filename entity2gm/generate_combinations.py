epoch = 600
mincount = 10
batchsize = 128
wout = 'wsame'
sperical = "True"
iter = 1
reg = "noreg"

# outf = open('commands.sh', 'w')
outf2 = open('commands+logs.txt', 'w')

for dim in [40]:
    for margin in [4.0]:
        for grad in ['rms']:
            for dropout in [0.6]:
                for rel in ["True"]:
                    for lr in [0.15, 0.1, 0.05, 0.01]:
                        for lower_sig in [0.0001, 0.001]:
                            for upper_sig in [100.0, 50.0]:
                                for glove in [' --glove', '']:
                                    for dataset in ['ag']:

                                        command = 'python entity2gm4.py --train_data data3/www-'+dataset+'-all --iter 1 --num_mixtures 1 --spherical True --consine_schedule --embedding_size ' + str(dim) + ' --epochs_to_train 600 --objective_threshold ' + str(
                                            margin) + ' --var_scale 0.05 --save_path /mnt/scratch/boyang/actors/modelfiles_versatility_test --learning_rate ' + str(lr) + ' --' + grad + ' --min_count ' + str(mincount) + ' --batch_size 128 --checkpoint_interval 1500 --dropout ' + str(
                                                dropout) + ' --concurrent_steps 4 --rel ' + rel + ' --normclip True --upper_sig ' + str(upper_sig) + ' --lower_sig ' + str(lower_sig) + glove

                                        # if rel == 'False':
                                        #     relstr = 'norel'
                                        # else:
                                        #     relstr = 'rel'
                                        # logfile = '/mnt/scratch/boyang/actors/modelfiles_versatility/www-'+dataset+'-all_t600_dim'+str(dim)+'_b128_min10_'+str(grad)+'_spherical_wsame_lr'+str(
                                        #     lr)+'_margin'+str(margin)+'_drop'+str(dropout)+'_clipTrue_'+str(lower_sig)+'-'+str(upper_sig)+'_'+relstr+'_noreg__1/loss.log'

                                        logfile = '/mnt/scratch/boyang/actors/modelfiles_versatility/www-'+dataset+'loss.log'
                                        # outf.write(command + '\n')
                                        outf2.write(command + '\n')
                                        outf2.write(logfile+'\n')

                                    # command = 'python entity2gm4.py --train_data data3/www-agp-all --iter 1 --num_mixtures 1 --spherical True --consine_schedule --embedding_size ' + str(dim) + ' --epochs_to_train 600 --objective_threshold ' + str(
                                    #    margin) + ' --var_scale 0.05 --save_path /mnt/scratch/boyang/actors/modelfiles_versatility_test --learning_rate ' + str(lr) + ' --' + grad + ' --min_count ' + str(mincount) + ' --batch_size 128 --checkpoint_interval 1500 --dropout ' + str(
                                    #        dropout) + ' --concurrent_steps 4 --rel False --normclip True --upper_sig ' + str(upper_sig) + ' --lower_sig ' + str(lower_sig) + glove
                                    #logfile = '/mnt/scratch/boyang/actors/modelfiles_versatility/www-'+dataset+'loss.log'
                                    #outf2.write(command + '\n')
                                    # outf2.write(logfile+'\n')

# outf.close()
outf2.close()
