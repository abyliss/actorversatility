#!/bin/bash
python -u entity2gm4.py --normclip --train_data data3/www-agp-train1 --iter 10 --num_mixtures 1 --spherical True --embedding_size 40 --epochs_to_train 400 --var_scale 1.0 --save_path modelfiles4/ --learning_rate 0.1 --adagrad --min_count 10 --batch_size 128 --max_to_keep 5 --checkpoint_interval 1500 --dropout 0.5 --concurrent_steps 4 --reg_beta 0.01  --rel True --reg_gamma 0.1 --reg_delta 0.1 
# > run.log
