This repository contains the code and data for the paper "Understanding Actors and Evaluating Personae with Gaussian Embeddings". 

## Content Table

The folder `entity2gm` contains the code for Gaussian embeddings for actors, movies, keywords, and personae. 

The folder `versatility-eval` contains the code for evaluating an list of actor versatility against the gold standard annotated by four Hollywood-trained actors. 

The folder `TransE` contains the TransE baseline. 

## Bibtex

If you use code from this repository, please cite the following paper. 

Hannah Kim, Denys Katerenchuk, Daniel Billet, Jun Huan, Haesun Park, and Boyang Li. Understanding Actors and Evaluating Personae with Gaussian Embeddings. In *the Proceedings of the 33rd AAAI Conference on Artificial Intelligence (AAAI)*. Honolulu, Hawaii. 2019. 

```
@inproceedings{Kim-understanding-actors:2019,
  author       = {Hannah Kim and Denys Katerenchuk and Daniel Billet and Jun Huan and Haesun Park and Boyang Li}, 
  title        = {Understanding Actors and Evaluating Personae with Gaussian Embeddings},
  booktitle    = {Proceedings of the 33rd AAAI Conference on Artificial Intelligence (AAAI)},
  year         = 2019,
}
```