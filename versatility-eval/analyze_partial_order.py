from openpyxl import Workbook
from openpyxl import load_workbook
import numpy as np
import sys

TABLE_LENGTH = 200
DECISION_PARALELL = 0
DECISION_GREATER = 1
DECISION_LESS = -1

def extract_actor_names(xlsx_file):
  print "reading from file: " + xlsx_file

  wb = load_workbook(filename=xlsx_file, read_only=True)
  sheet_names = wb.get_sheet_names()
  if 'groups' not in sheet_names:
    print xlsx_file + ' does not contain sheet named "groups"...exiting prematurely...'
    sys.exit(2)

  sheet = wb['groups']

  names = [] 

  row_idx = 0
  for row in sheet.rows:
    if row_idx % 2 == 0 and row_idx < TABLE_LENGTH:
      for cell in row[1:6]:
        if cell.value == None :
          print 'warning: row ' + str(row_idx+1) + ': ' + str([c.value for c in row]) + ' contains a None. Something is wrong with the file format.'
        names.append(cell.value.strip())
    row_idx += 1

  # close the file
  wb.close()
    
  names = sorted(list(set(names)))
  length = len(names)
  print 'we have a total of ' + str(length) + ' actors.'
  
  # a dictionary mapping actor name to id
  actor_ids = dict(zip(names, range(len(names))))
  return actor_ids


def find_all_comparisons(name1, name2, records):
  #print name1
  #print name2
  results = []
  i = 1
  for row in records:
    names = [x[0] for x in row]
    if name1 in names and name2 in names:
      # found a relevant line
      results.append([i, row])
    i += 1
  return results

def read_matrix(xlsx_file, actor_ids): 

  print "reading from file: " + xlsx_file

  wb = load_workbook(filename=xlsx_file, read_only=True)
  sheet_names = wb.get_sheet_names()
  if 'groups' not in sheet_names:
    print xlsx_file + ' does not contain sheet named "groups"...exiting prematurely...'
    sys.exit(2)

  sheet = wb['groups']

  # build numpy matrix
  length = len(actor_ids)
  comp_matrix = np.zeros([length, length])

  all_rows = []

  for i in range(1, TABLE_LENGTH, 2):
    # extracting information contained in every two rows
    local_names = []
    local_rankings = []
    for j in range(2,7):
      name_cell = sheet.cell(row = i, column = j)
      rank_cell = sheet.cell(row = i+1, column = j)
      local_names.append(name_cell.value.strip())
      local_rankings.append(rank_cell.value)
    
    # analyze the info
    # first, sort according to ranking
    local = sorted(zip(local_names, local_rankings), key=lambda t: t[1])
    all_rows.append(local)

    # then record the result in the comparison matrix
    for k in range(5):
      first_item = local[k]
      for kk in range(k,5,1):
        second_item = local[kk]
        if (first_item[1] < second_item[1]):
          big_id = actor_ids[second_item[0]]
          small_id = actor_ids[first_item[0]]
          if comp_matrix[big_id, small_id] <= -1:
            print 'Warning: inconsistent comparison on row ' + str(i)  
            print str(first_item) + ' < ' + str(second_item)
            all_comps = find_all_comparisons(first_item[0], second_item[0], all_rows)
            print '***Records involve the two actors***'
            print [str(x[0]*2-1) + 'th row: ' + str(x[1]) for x in all_comps]
          # add these values no matter what    
          comp_matrix[big_id, small_id] += 1
          comp_matrix[small_id, big_id] -= 1

  # close the file
  wb.close()

  return comp_matrix, all_rows

def read_pair_decisions(xlsx_file, actor_ids): 

  print "reading from file: " + xlsx_file

  wb = load_workbook(filename=xlsx_file, read_only=True)
  sheet_names = wb.get_sheet_names()
  if 'groups' not in sheet_names:
    print xlsx_file + ' does not contain sheet named "groups"...exiting prematurely...'
    sys.exit(2)

  sheet = wb['groups']

  # build numpy matrix
  length = len(actor_ids)
  comp_matrix = np.zeros([length, length])

  all_pairs = dict()

  for i in range(1, TABLE_LENGTH, 2):
    # extracting information contained in every two rows
    local_names = []
    local_rankings = []
    for j in range(2,7):
      name_cell = sheet.cell(row = i, column = j)
      rank_cell = sheet.cell(row = i+1, column = j)
      local_names.append(name_cell.value.strip())
      local_rankings.append(rank_cell.value)

    # analyze the info
    # first, sort according to ranking
    local = sorted(zip(local_names, local_rankings), key=lambda t: t[1])
    for j in range(len(local)):
      for k in range(j):
        name1 = local[j][0]
        name2 = local[k][0]
        decision = DECISION_PARALELL
        if local[j] < local[k]:
          decision = DECISION_LESS
        elif local[j] > local[k]:
          decision = DECISION_GREATER
        
        if name1 > name2:
          # invert the names and the decision
          temp = name1
          name1 = name2
          name2 = temp
          decision = decision * -1

        
        dict[(name1, name2)] = decision
        
  return all_pairs

def get_all_pairs(infile, actor_ids):
  
  #actor_ids = extract_actor_names(infile)
  comp_arr, all_records = read_matrix(infile, actor_ids)
  
  all_pairs = []
  for record in all_records:
    for i in range(5):
      for j in range(i):
        # check alphabetic ordering
        if record[i][0] > record[j][0]:
          all_pairs.append((record[i][0],record[j][0]))
        else:
          all_pairs.append((record[j][0],record[i][0]))  
  
  pair_set = set(all_pairs)
  return list(pair_set)
  #print(pair_set)
  #print(len(pair_set))

def read_pairs_votes():
  inputfiles = ["comp1.xlsx", "comp2.xlsx", "comp3.xlsx", "comp4.xlsx"]

  #if len(sys.argv) < 2:
  #  print "Usage: python analyze_partial_order.py <xlsx filename>"
  #  sys.exit(1)
  #else:
  #  inputfile = sys.argv[1].strip()
  pair_votes = dict()  
  results = []
  actor_ids = extract_actor_names(inputfiles[0])
  all_pairs = get_all_pairs(inputfiles[0], actor_ids)

  for infile in inputfiles:    
    comp_arr, _ = read_matrix(infile, actor_ids)
    comp_arr[comp_arr > 1] = 1
    comp_arr[comp_arr < -1] = -1
    results.append(comp_arr)

  for pair in all_pairs:
    answers = np.zeros(3)
    for array in results:
      id1 = actor_ids[pair[0]]  
      id2 = actor_ids[pair[1]]
      decision = array[id1, id2] + 1
      answers[int(decision)] += 1
    pair_votes[pair] = answers
    if np.sum(answers) != 4:
      print 'warning: not 4 votes' 
      print pair
      print answers
    if answers[1] != 0:
      print 'a vote for parallel' 
      print pair
      print answers
  
  return pair_votes
  #print pair_votes
  #final_array = results[0] + results[1] + results[2] + results[3]
  #np.savetxt('array.csv',final_array, fmt='%d', delimiter=',')

def fleiss_kappa(pair_votes):
  n = len(pair_votes)
  m = 4
  vote_array = np.stack(pair_votes.values())

  sum_squares = np.sum(vote_array**2)  
  pa = (sum_squares - n * m) / ((m-1) * m * n)

  qj = np.sum(vote_array, axis = 0) / (n * m)
  ps = np.sum(qj**2)
  
  return (pa-ps) / (1-ps)


def find_majority_pairs(final_array, actor_ids):
  'given the array, find out the names of actor pairs that has a value greater than 1'
  reverse_dict = {}
  for key in actor_ids.keys():
    value = actor_ids[key]
    reverse_dict[value] = key

  N = len(actor_ids)
  pairs = []
  for i in range(N):
    for j in range(N):
      if final_array[i, j] > 0:
        big_name = reverse_dict[i]
        small_name = reverse_dict[j]
        pairs.append([big_name, small_name])

  return pairs

def print_pairs_to_file(pairs, filename):
  f = open(filename, 'w') 
  for p in pairs:
    f.write(p[0] + ', ' + p[1] + '\n')

  f.close()

def main2():
  'run this method for computing fleiss\'s kappa'
  pair_votes = read_pairs_votes()
  kappa = fleiss_kappa(pair_votes)
  print kappa



def main():
  'this outputs ordered pairs: (big, small)'

  inputfiles = ["comp1.xlsx", "comp2.xlsx", "comp3.xlsx", "comp4.xlsx"]

  #if len(sys.argv) < 2:
  #  print "Usage: python analyze_partial_order.py <xlsx filename>"
  #  sys.exit(1)
  #else:
  #  inputfile = sys.argv[1].strip()
  
  results = []
  actor_ids = extract_actor_names(inputfiles[0])

  for infile in inputfiles:    
    comp_arr, _ = read_matrix(infile, actor_ids)
    comp_arr[comp_arr > 1] = 1
    comp_arr[comp_arr < -1] = -1
    results.append(comp_arr)
    #np.savetxt('array.csv',comp_matrix, fmt='%d', delimiter=',')
    #partial_order = analyze_matrix(comp_matrix)

  final_array = results[0] + results[1] + results[2] + results[3]
  #np.savetxt('array.csv',final_array, fmt='%d', delimiter=',')
  unique, counts = np.unique(final_array, return_counts=True)
  print 'unique:' 
  print unique
  print 'counts:'
  print counts
  majority = counts[0] + counts[1] + counts[2] + counts[3]
  print 'we have a majority opinion on ' + str(majority) + ' pairs'
  
  pairs = find_majority_pairs(final_array, actor_ids)
  print_pairs_to_file(pairs, 'ordered_pairs.txt')

if __name__ == "__main__":
    main()
    #get_all_pairs()
