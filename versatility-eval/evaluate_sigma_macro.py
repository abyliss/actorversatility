import numpy as np
import sys
import csv
from collections import defaultdict
import random
import scipy.stats
import argparse
import os
import re

regex = re.compile(r"_t[0-9]+_", re.IGNORECASE)


def read_sigma_ranking(filename):
    actor_number = []
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            if row[0].startswith('p'):
                array = row[0].split('_')
                name = ' '.join(array[2:])
                array = name.split(':')
                name = array[0].strip()
                actor_number.append([name, row[2]])

    sorted_actors = sorted(actor_number, key=lambda x: x[1])

    dict = {}
    for i in range(len(sorted_actors)):
        name = sorted_actors[i][0]
        dict[name] = i

    return dict


def read_gold_ranking(filename):
    pairs = []

    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            pairs.append(row)

    return pairs


def eval_ranking(ranking, gold):
    correct = 0
    total = 0
    missing = 0
    for pair in gold:
        big = pair[0].strip()
        small = pair[1].strip()
        if not big in ranking:
            # print 'missing key ' + big
            missing += 1
        elif not small in ranking:
            # print 'missing key ' + small
            missing += 1
        elif ranking[big] < ranking[small]:
            # this is a correct pair. Note a smaller ranking indicates greater versatility
            correct += 1
            total += 1
            # print 'correct: ' + big + " > " + small
        else:
            # print 'wrong: ' + big + " < " + small
            total += 1

    return correct / float(total)


def find_missing(ranking, gold):
    missing = 0
    all_actor_set = set()
    for pair in gold:
        all_actor_set.add(pair[0].strip())
        all_actor_set.add(pair[1].strip())

    for name in all_actor_set:
        if not name in ranking:
            missing += 1

    return missing


def topological_sort(pairs):
    answer = []
    ancestors_count = defaultdict(int)
    successor_map = defaultdict(list)

    zero_ancestors = set()
    for pair in pairs:
        first = pair[0].strip()
        zero_ancestors.add(first)

    for pair in pairs:
        first = pair[0].strip()
        second = pair[1].strip()
        ancestors_count[second] += 1
        successor_map[first].append(second)
        if second in zero_ancestors:
            zero_ancestors.remove(second)

    for k in zero_ancestors:
        ancestors_count[k] = 0
    # print ancestors_count
    # print successor_map

    while len(ancestors_count) > 0:

        # find which element to pop
        # the number of items is small, so no need to use a heap
        fringe = [k for k, v in ancestors_count.items() if v == 0]
        print fringe
        ridx = random.randint(0, len(fringe)-1)
        print "index " + str(ridx) + " from a list of " + str(len(fringe)) + " items"
        pop = fringe[ridx]
        answer.append(pop)
        print "popped " + pop

        # do some maintenance
        del ancestors_count[pop]
        if pop in successor_map.keys():
            for successor in successor_map[pop]:
                ancestors_count[successor] -= 1
                print "ancestor count for " + successor + " is now " + str(ancestors_count[successor])

    return answer


def mid_rank_sort(pairs):
    answer = {}
    ancestors_count = defaultdict(int)
    successor_map = defaultdict(list)

    zero_ancestors = set()
    for pair in pairs:
        first = pair[0].strip()
        zero_ancestors.add(first)

    for pair in pairs:
        first = pair[0].strip()
        second = pair[1].strip()
        ancestors_count[second] += 1
        successor_map[first].append(second)
        if second in zero_ancestors:
            zero_ancestors.remove(second)

    for k in zero_ancestors:
        ancestors_count[k] = 0
    # print ancestors_count
    # print successor_map

    last_rank = 0

    while len(ancestors_count) > 0:

        # find all fringe elements, which are tied
        fringe = [k for k, v in ancestors_count.items() if v == 0]
        mid_rank = (1+len(fringe))/2.0 + last_rank
        for pop in fringe:
            answer[pop] = mid_rank

            # do some maintenance
            del ancestors_count[pop]
            if pop in successor_map.keys():
                for successor in successor_map[pop]:
                    ancestors_count[successor] -= 1
                    # print "ancestor count for " + successor + " is now " + str(ancestors_count[successor])

        last_rank += len(fringe)

    return answer


def read_ranking_as_dict(filename):
    dd = {}
    i = 1
    with open(filename, 'r') as infile:
        lines = infile.readlines()
        for l in lines:
            dd[l.strip()] = i
            i += 1
    return dd


def main(infile, gold_file):
    our_ranking = read_sigma_ranking(infile)
    gold_ranking = read_gold_ranking(gold_file)
    ratio = eval_ranking(our_ranking, gold_ranking)
    print 'pairwise correct %: ' + str(ratio)
    # print 'missing actors: ' + str(find_missing(our_ranking, gold_ranking))
    return ratio


def main2(gold_file):
    """
    computes ranking correlation between the provided total order and
    the gold standard list
    """
    gold_ranking = read_gold_ranking(gold_file)
    for i in range(100):
        answer = topological_sort(gold_ranking)
        with open('./topo_sorts/sort_%03d.txt' % (i+1), 'w') as outfile:
            for c in answer:
                outfile.write(c + "\n")


def main3(gold_file):
    """
    verify that the topo sorts are correct
    """
    gold_ranking = read_gold_ranking(gold_file)

    for i in range(100):
        filename = './topo_sorts/sort_%03d.txt' % (i+1)
        dd = read_ranking_as_dict(filename)
        ratio = eval_ranking(dd, gold_ranking)
        print("agreement: " + str(ratio))
        assert(ratio == 1.0)


def main4_mean_rank_with_toposort(infile_name, dir_name):
    """
    compute the average rank correlation between an input ranking and all the topological sorts
    """
    our_ranking = read_sigma_ranking(infile_name)
    corr = 0
    for i in range(100):
        filename = './%s/sort_%03d.txt' % (dir_name, i+1)
        dd = read_ranking_as_dict(filename)
        # print('reading from ' + filename)
        a = []
        b = []
        for k in dd.keys():
            if k in our_ranking.keys():
                a.append(our_ranking[k])
                b.append(dd[k])

        corr += scipy.stats.spearmanr(a, b)[0]

    print('average of topological sort. correlation = %.5f' % (corr/100.0))
    return corr/100.0


def main5_mid_rank(infile_name, gold_file):
    """
    compute the rank correlation with the mid rank method.
    That is, all tied items receive the mean of their rank
    (1, 2, 2, 4) becomes (1, 2.5, 2.5, 4)
    """
    gold_pairs = read_gold_ranking(gold_file)
    gold_ranking = mid_rank_sort(gold_pairs)
    our_ranking = read_sigma_ranking(infile_name)
    a = []
    b = []
    for k in gold_ranking.keys():
        if k in our_ranking.keys():
            a.append(our_ranking[k])
            b.append(gold_ranking[k])

    corr = scipy.stats.spearmanr(a, b)[0]

    print('midrank method. correlation = %.5f' % corr)
    return corr


if __name__ == "__main__":
    #  gold_file = 'gold_pairs.csv'
    #  parser = argparse.ArgumentParser(description='Compute correlation with gold versatility ranking.')
    #  parser.add_argument('input', type=str,
    #                   help='the filename containing the user ranking')
    #
    #  args = parser.parse_args()
    #  filename = args.input
    #  #print(filename)
    #  main5_mid_rank(filename, gold_file)
    #  main4_mean_rank_with_toposort(filename)
    #  main(filename, gold_file)

    # macro for all folders
    parser = argparse.ArgumentParser(description='evaluate all subfolders')
    parser.add_argument('model_path', help='path for saved models')
    args = parser.parse_args()

    model_dir = args.model_path  # '../entity2gm/modelfiles_sigmas2/'
    gold_file = 'gold_validation_pairs.csv'
    gold_file2 = 'gold_test_pairs.csv'
    dir_name = 'topo_sorts_val'  # this is for validation -- Albert

    with open(os.path.join(model_dir, 'versatility.csv'), 'w') as f:
        writer = csv.writer(f, quoting=csv.QUOTE_NONE)
        writer.writerow(['modelfile', 'topo sort corr val', 'correct % val',
                         'mid rank corr val', 'topo sort corr test', 'correct % test', 'mid rank corr test'])
        for fold in next(os.walk(model_dir))[1]:
            print(model_dir+fold)
            # try:
            filename = os.path.join(model_dir, fold, 'embed_sigma.csv')
            corr_t1 = main4_mean_rank_with_toposort(
                filename, 'topo_sorts_val')
            corr_m1 = main5_mid_rank(filename, gold_file)
            ratio = main(filename, gold_file)

            corr_t2 = main4_mean_rank_with_toposort(
                filename, 'topo_sorts_test')
            corr_m2 = main5_mid_rank(filename, gold_file2)
            ratio2 = main(filename, gold_file2)
            print '---------------------------------correct %: ' + str(ratio)
            writer.writerow([fold, str(
                corr_t1), str(ratio), str(corr_m1), str(corr_t2), str(ratio2), str(corr_m2)])
            # except:
            #    print '---------------------------------error'
        for filename in ['versatility-genre.csv', 'versatility-keyword.csv', 'versatility-persona.csv']:
            corr_t1 = main4_mean_rank_with_toposort(filename, 'topo_sorts_val')
            corr2 = main5_mid_rank(filename, gold_file)
            ratio = main(filename, gold_file)
            corr_t2 = main4_mean_rank_with_toposort(
                filename, 'topo_sorts_test')
            corr3 = main5_mid_rank(filename, gold_file2)
            ratio2 = main(filename, gold_file2)
            print '---------------------------------correct %: ' + str(ratio)
            writer.writerow([filename, str(corr_t1), str(ratio), str(
                corr2), str(corr_t2), str(ratio2), str(corr3)])
