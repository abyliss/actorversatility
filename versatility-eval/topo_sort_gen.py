from collections import defaultdict
import random
import csv


def read_gold_ranking(filename):
    pairs = []

    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            pairs.append(row)

    return pairs


def topological_sort(pairs):
    answer = []
    ancestors_count = defaultdict(int)
    successor_map = defaultdict(list)

    zero_ancestors = set()
    for pair in pairs:
        first = pair[0].strip()
        zero_ancestors.add(first)

    for pair in pairs:
        first = pair[0].strip()
        second = pair[1].strip()
        ancestors_count[second] += 1
        successor_map[first].append(second)
        if second in zero_ancestors:
            zero_ancestors.remove(second)

    for k in zero_ancestors:
        ancestors_count[k] = 0
    # print ancestors_count
    # print successor_map

    while len(ancestors_count) > 0:

        # find which element to pop
        # the number of items is small, so no need to use a heap
        fringe = [k for k, v in ancestors_count.items() if v == 0]
        # print fringe
        ridx = random.randint(0, len(fringe)-1)
        # print "index " + str(ridx) + " from a list of " + str(len(fringe)) + " items"
        pop = fringe[ridx]
        answer.append(pop)
        # print "popped " + pop

        # do some maintenance
        del ancestors_count[pop]
        if pop in successor_map.keys():
            for successor in successor_map[pop]:
                ancestors_count[successor] -= 1
                # print "ancestor count for " + successor + " is now " + str(ancestors_count[successor])

    return answer


def main2():
    """
    computes ranking correlation between the provided total order and 
    the gold standard list
    """
    gold_ranking = read_gold_ranking('gold_test_pairs.csv')
    for i in range(100):
        answer = topological_sort(gold_ranking)
        with open('./topo_sorts_test/sort_%03d.txt' % (i+1), 'w') as outfile:
            for c in answer:
                outfile.write(c + "\n")


if __name__ == "__main__":
    main2()
