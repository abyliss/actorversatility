# this program splits the data into validation and test
# we have corrected the problem that test data may sneak into the
# validation data.
# First we pick a set of validation actors. All comparisons betwee
# the validation actors go into the validation set. Other comparisons
# form the test set
import random


def read_all_pairs(file):
    pairs = []
    with open(file, 'r') as inf:
        lines = inf.readlines()
    for line in lines:
        arr = line.split(',')
        actor1 = arr[0].strip()
        actor2 = arr[1].strip()
        pairs.append((actor1, actor2))

    return pairs


def find_all_actors(pairs):
    actors = []
    for a1, a2 in pairs:
        actors.append(a1)
        actors.append(a2)

    return list(set(actors))


def write_pairs(file, pairs):
    with open(file, 'w') as outf:
        for a1, a2 in pairs:
            outf.write(a1 + ', ' + a2 + '\n')


def main():
    pairs = read_all_pairs('gold_pairs.csv')
    actors = find_all_actors(pairs)
    # print(actors)
    random.shuffle(actors)
    n = len(actors)
    half = n // 2
    selected = actors[0:half]

    val = []
    test = []
    for pair in pairs:
        a1 = pair[0]
        a2 = pair[1]
        if (a1 in selected and a2 in selected):
            val.append(pair)
        else:
            test.append(pair)
    write_pairs('gold_validation_pairs.csv', val)
    write_pairs('gold_test_pairs.csv', test)
    print('validation pairs = %d' % len(val))
    print('test pairs = %d' % len(test))


if __name__ == "__main__":
    main()
