#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0
for iter in 1 2 3
do
  for dim in 40
  do
    for epoch in 400
    do
      for margin in 4
      do
        for batchsize in 128
        do
	        for lr in 0.05
	        do
              python main.py --data_dir ../data/data-baseline-min10-test/ --output_dir ../output/data-baseline-min10-test-t${epoch}-d${dim}-m${margin}-b${batchsize}-l${lr}_${iter}/ --embedding_dim ${dim} --margin_value ${margin} --batch_size ${batchsize} --learning_rate ${lr} --n_generator 24 --n_rank_calculator 24 --eval_freq 100 --max_epoch ${epoch}
          done
        done
      done
    done
  done
done