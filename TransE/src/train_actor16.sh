#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0
for iter in 2
do
  for dim in 50
  do
    for epoch in 600
    do
      for margin in 4
      do
        for batchsize in 128
        do
	        for lr in 0.01
	        do
              python main.py --data_dir ../data/data-baseline-min10-val/ --output_dir ../output/data-baseline-min10-val-t${epoch}-d${dim}-m${margin}-b${batchsize}-l${lr}_${iter}/ --embedding_dim ${dim} --margin_value ${margin} --batch_size ${batchsize} --learning_rate ${lr} --n_generator 24 --n_rank_calculator 24 --eval_freq 100 --max_epoch ${epoch}
          done
        done
      done
    done
  done
done