#!/usr/bin/env bash
export CUDA_VISIBLE_DEVICES=0
for iter in 2
do
  for dim in 50
  do
    for epoch in 400
    do
      for margin in 1 2
      do
        for batchsize in 128
        do
	        for lr in 0.01 0.03 0.05
	        do
              python main.py --data_dir ../data/data-baseline-min10-agp-val/ --output_dir ../output/data-baseline-min10-val-t${epoch}-d${dim}-m${margin}-b${batchsize}-l${lr}_${iter}/ --embedding_dim ${dim} --margin_value ${margin} --batch_size ${batchsize} --learning_rate ${lr} --n_generator 2 --n_rank_calculator 6 --eval_freq 100 --max_epoch ${epoch}
          done
        done
      done
    done
  done
done
