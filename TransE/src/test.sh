#!/usr/bin/env bash
export CUDA_VISIBLE_DEVICES=0
python main.py --data_dir ../data/data-baseline-min10-agp-val/ --output_dir ../output/test/ --embedding_dim 30 --margin_value 1 --batch_size 10 --learning_rate 0.005 --n_generator 24 --n_rank_calculator 24 --eval_freq 100 --max_epoch 10
