import tensorflow as tf

'''
This provides a mechanism for filtering zero paddings for relations
The zero paddings will always yield zero embedding vectors
'''

bound = 0.5
entity = 10
embedding_dim = 20

embedding = tf.get_variable(name='entity',
    shape=[entity, embedding_dim],
    initializer=tf.random_uniform_initializer(minval=-bound, maxval=bound))
# zero_embedding = tf.get_variable(name='entity',
#     shape=[1, embedding_dim],
#     initializer=tf.zeros_initializer())

triple_pos = tf.placeholder(dtype=tf.int32, shape=[None, 5])
b = tf.get_variable("b", shape=[2, 3], dtype=tf.int32)
a = triple_pos[:, 2:5]
comparison = tf.equal(a, 0)
print(comparison)
indices = tf.cast(tf.where(comparison, tf.zeros_like(a), tf.ones_like(b)), tf.float32)
rel_mask = tf.transpose(tf.tile(tf.expand_dims(indices, axis=1), [1, embedding_dim, 1]), [0, 2, 1])
print(indices)
rel_emb = tf.nn.embedding_lookup(embedding, triple_pos[:, 2:5])

product = tf.multiply(rel_emb, rel_mask)
#masked_emb = tf.boolean_mask(rel_emb, rel_mask)
remb = tf.reduce_sum(product, axis=1)

gpu_config = tf.GPUOptions(allow_growth=True)
sess_config = tf.ConfigProto(gpu_options=gpu_config)

with tf.Session(config=sess_config) as sess:
    tf.global_variables_initializer().run()
    _triple_pos = [[1, 2, 3, 0, 0], [5, 4, 1, 0, 2]]
    _rel_mask = [[True, False, True]]
    e_, t_, p_= sess.run(fetches=[rel_emb, rel_mask, remb],
            feed_dict={
                triple_pos:_triple_pos})
    print(e_)
    #print(op)
    print(t_.shape)
    print(p_)